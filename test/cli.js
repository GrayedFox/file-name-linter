const childProcess = require('child_process')
const describe = require('mocha').describe
const expect = require('must')
const it = require('mocha').it
const path = require('path')

describe('CLI', () => {
  const run = (args, callback) => {
    const cli = childProcess.fork(path.join(__dirname, '..', 'lib', 'cli'), args, { silent: true })

    let stdoutBuffer = ''
    cli.stdout.on('data', (data) => {
      stdoutBuffer += data
    })

    let stderrBuffer = ''
    cli.stderr.on('data', (data) => {
      stderrBuffer += data
    })

    cli.on('close', (code) => {
      callback(code, stdoutBuffer, stderrBuffer)
    })
  }

  it('must show usage without arguments', (done) => {
    run([], (code, stdout, stderr) => {
      expect(code).to.equal(1)
      expect(stdout).to.equal('')
      expect(stderr).to.equal('Usage: file-name-linter config.json\n')
      done()
    })
  })

  it('must show usage with too many arguments', (done) => {
    run(['one', 'two'], (code, stdout, stderr) => {
      expect(code).to.equal(1)
      expect(stdout).to.equal('')
      expect(stderr).to.equal('Usage: file-name-linter config.json\n')
      done()
    })
  })

  it('must show error for non-existent config file', (done) => {
    const fileName = 'non-existent.json'
    run([fileName], (code, stdout, stderr) => {
      expect(code).to.equal(1)
      expect(stdout).to.equal('')
      expect(stderr).to.match(`Cannot find module '${path.resolve(fileName)}'\n`)
      done()
    })
  })

  it('must do nothing for empty config file', (done) => {
    const fileName = path.join(__dirname, 'fixtures', 'empty-config.json')
    run([fileName], (code, stdout, stderr) => {
      expect(code).to.equal(0)
      expect(stdout).to.equal('')
      expect(stderr).to.equal('The config seems to be empty!\n')
      done()
    })
  })

  it('must lint files according to JSON config', (done) => {
    const fileName = path.join(__dirname, 'fixtures', 'config.json')
    run([fileName], (code, stdout, stderr) => {
      expect(stdout.split('\n').sort()).to.eql([
        '',
        ` 🏁  Finished linting ${path.join('test', 'fixtures', 'fails', path.sep)}`,
        ` 🏁  Finished linting ${path.join('test', 'fixtures', 'scripts', path.sep)}`
      ])
      expect(stderr.split('\n').sort()).to.eql([
        '',
        ` ✖  ${path.join('test', 'fixtures', 'fails', 'linting')} does not match /some-expression/!`,
        "empty-directory ENOENT: no such file or directory, scandir 'empty-directory'",
        "non-existent ENOENT: no such file or directory, scandir 'non-existent'"
      ])
      expect(code).to.equal(1)
      done()
    })
  })

  it('must exit with 0 if no linting errors occurred', (done) => {
    const fileName = path.join(__dirname, 'fixtures', 'no-errors-config.json')
    run([fileName], (code, stdout, stderr) => {
      expect(stderr).to.equal('')
      expect(stdout.split('\n').sort()).to.eql([
        '',
        ` 🏁  Finished linting ${path.join('test', 'fixtures', 'scripts', path.sep)}`
      ])
      expect(code).to.equal(0)
      done()
    })
  })

  it('must lint files according to JavaScript config', (done) => {
    const fileName = path.join(__dirname, 'fixtures', 'config.js')
    run([fileName], (code, stdout, stderr) => {
      expect(stderr.split('\n').sort()).to.eql([
        '',
        ` ✖  ${path.join('fails', 'linting')} does not match /^[a-z\\/]+\\.js|json$/!`
      ])
      expect(stdout.split('\n').sort()).to.eql([
        '',
        ` 🏁  Finished linting ${path.join(__dirname, 'fixtures')}`
      ])
      expect(code).to.equal(1)
      done()
    })
  })
})

