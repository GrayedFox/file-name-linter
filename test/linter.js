const beforeEach = require('mocha').beforeEach
const describe = require('mocha').describe
const it = require('mocha').it
const expect = require('must')
const Linter = require('..').Linter

describe('Linter', () => {
  const testFileNamePattern = '\\d+'
  let linter

  beforeEach(() => {
    linter = new Linter(testFileNamePattern)
  })

  it('must pass check for valid names', (done) => {
    linter.errorCallback = (fileNamePattern, fileName) => done(new Error(`Expected ${fileName} to match ${fileNamePattern}!`))
    expect(linter.check('1234')).to.equal(true)
    done()
  })

  it('must fail check for invalid names', (done) => {
    const invalidFileName = 'abc'
    linter.errorCallback = (fileNamePattern, fileName) => {
      expect(fileNamePattern.toString()).to.equal(new RegExp(testFileNamePattern).toString())
      expect(fileName).to.equal(invalidFileName)
      done()
    }
    expect(linter.check(invalidFileName)).to.equal(false)
  })
})
