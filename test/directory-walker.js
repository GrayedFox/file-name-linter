const afterEach = require('mocha').afterEach
const beforeEach = require('mocha').beforeEach
const describe = require('mocha').describe
const expect = require('must')
const fs = require('fs')
const it = require('mocha').it
const path = require('path')
const DirectoryWalker = require('..').DirectoryWalker

const noop = Function.prototype

describe('DirectoryWalker', () => {
  const emptyDirectory = path.join(__dirname, 'fixtures', 'empty-directory')
  let directoryWalker

  beforeEach((done) => {
    directoryWalker = new DirectoryWalker()
    fs.mkdir(emptyDirectory, done)
  })

  afterEach((done) => {
    fs.rmdir(emptyDirectory, done)
  })

  it('must list existing files', (done) => {
    const directory = path.join(__dirname, 'fixtures')
    const files = []

    directoryWalker.errorCallback = (rootDirectory, err) => done(err)
    directoryWalker.fileCallback = (rootDirectory, filePath) => {
      expect(rootDirectory).to.equal(directory)
      files.push(path.relative(rootDirectory, filePath))
    }
    directoryWalker.finishCallback = (rootDirectory) => {
      expect(rootDirectory).to.equal(directory)
      expect(files.sort()).to.eql([
        'config.js',
        'config.json',
        'empty-config.json',
        path.join('fails', 'linting'),
        'no-errors-config.json',
        path.join('scripts', 'one.js'),
        path.join('scripts', 'two.js')
      ])
      done()
    }

    directoryWalker.walk(directory)
  })

  it('must fail for non-existent directory', (done) => {
    const directory = 'does-not-exist'

    directoryWalker.errorCallback = (rootDirectory, err) => {
      expect(rootDirectory).to.equal(directory)
      const expectedError = new Error()
      expectedError.errno = -2
      expectedError.code = 'ENOENT'
      expectedError.syscall = 'scandir'
      expectedError.path = directory
      expect(err).to.eql(expectedError)
      done()
    }
    directoryWalker.fileCallback = noop
    directoryWalker.finishCallback = noop

    directoryWalker.walk(directory)
  })

  it('must stop traversing when stat fails', (done) => {
    const directory = __dirname
    const error = new Error('moo')
    const failingCallback = () => done(new Error('Callback must not be called!'))

    directoryWalker.errorCallback = (rootDirectory, err) => {
      expect(rootDirectory).to.equal(directory)
      expect(err).to.equal(error)
      done()
    }
    directoryWalker.fileCallback = failingCallback
    directoryWalker.finishCallback = failingCallback

    directoryWalker.statCallback(directory, 'doesnt-matter', failingCallback, error, null)
  })

  it('must ignore anything but files and directories', (done) => {
    const directory = __dirname
    const stats = {
      isDirectory: () => false,
      isFile: () => false
    }
    const expectedFilePath = 'doesnt-matter'
    const failingCallback = () => done(new Error('Callback must not be called!'))

    directoryWalker.errorCallback = failingCallback
    directoryWalker.fileCallback = failingCallback
    directoryWalker.finishCallback = failingCallback

    directoryWalker.statCallback(directory, expectedFilePath, (rootDirectory, filePath) => {
      expect(rootDirectory).to.equal(directory)
      expect(filePath).to.equal(expectedFilePath)
      done()
    }, null, stats)
  })
})
