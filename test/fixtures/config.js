const path = require('path')

const config = {
  callbacks: {},
  rules: {}
}

const allowedFileExtensions = ['js', 'json']
config.rules[__dirname] = `^[a-z/]+\\.${allowedFileExtensions.join('|')}$`

config.callbacks.traverseFile = (defaultCallback, rootDirectory, filePath) => defaultCallback(rootDirectory, path.relative(rootDirectory, filePath))

module.exports = config
