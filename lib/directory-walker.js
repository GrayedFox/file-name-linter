const fs = require('fs')
const path = require('path')

const noop = Function.prototype

class DirectoryWalker {
  constructor (callbacks) {
    callbacks = callbacks || {}
    this.errorCallback = callbacks.errorCallback || console.error
    this.fileCallback = callbacks.fileCallback || console.log
    this.finishCallback = callbacks.finishCallback || noop
  }

  processDirectory (rootDirectory, directory, done) {
    fs.readdir(directory, this.readdirCallback.bind(this, rootDirectory, directory, done))
  }

  readdirCallback (rootDirectory, directory, done, err, files) {
    if (err) {
      return this.errorCallback(rootDirectory, err)
    }

    let pendingFiles = files.length

    if (!pendingFiles) {
      return done(rootDirectory, directory)
    }

    files.forEach((fileName) => {
      const filePath = path.join(directory, fileName)
      fs.stat(filePath, this.statCallback.bind(this, rootDirectory, filePath, () => {
        --pendingFiles
        if (!pendingFiles) {
          done(rootDirectory, directory)
        }
      }))
    })
  }

  statCallback (rootDirectory, filePath, done, err, stats) {
    if (err) {
      return this.errorCallback(rootDirectory, err)
    }

    if (stats.isDirectory()) {
      return this.processDirectory(rootDirectory, filePath, done)
    }

    if (stats.isFile()) {
      this.fileCallback(rootDirectory, filePath)
    }

    done(rootDirectory, filePath)
  }

  walk (rootDirectory) {
    this.processDirectory(rootDirectory, rootDirectory, this.finishCallback)
  }
}

module.exports = DirectoryWalker
